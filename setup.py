import os
from setuptools import setup


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname: str) -> str:
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name="pyDecline",
    version="0.0.1",
    author="John Aven",
    author_email="john.aven.phd@gmail.com",
    description="Oil & Gas Well Decline Curve Analysis (DCA) package.",
    license="Apache License, Version 2.0",
    keywords="DCA, decline curve",
    url="https://gitlab.com/john.aven.phd/pydecline",
    packages=['an_example_pypi_project', 'tests'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Reservoir Engineering",
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: Python :: 3 :: Only",
    ],
    install_requires=requirements,
)
