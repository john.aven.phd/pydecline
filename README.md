# pyDecline

pyDecline is for decline curve analysis. It includes the decline curve
computations based on various algorithms. The core functionalities are:

1. Compute the production rate at a specific point in time given a set
of parameters
2. Determine the best fit parameters for a decline curve given a
training dataset
3. Given a set of parameters, compute the decline curve


*Note*: This is a WIP.

