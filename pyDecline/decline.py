import numpy as np


class Decline:
    """
    Base class for decline curve analysis
    """

    def compute(self, time_start: float,
                time_end: float,
                production_at_start_time: float,
                **kwargs: dict) -> (np.ndarray, np.ndarray):
        """
        Computes the production

        Args:
            time_start: Time at which to start the decline curve computation
            time_end: Time at which to stop the decline curve computation
            production_at_start_time: The production rate at the starting time of the decline curve
            **kwargs: Various parameters used in computing the decline curve. Specific to the algorithm

        Returns:

        """

        raise ValueError('Base class. Please derive in child class')

