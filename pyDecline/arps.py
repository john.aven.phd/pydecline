import numpy as np

from pyDecline.decline import Decline


class Arps(Decline):

    def compute(self,
                time_start: float,
                time_end: float,
                production_at_start_time: float,
                **kwargs) -> (np.ndarray, np.ndarray):
        """
        Arps computation of decline curves.

        Args:
            time_start:
            time_end:
            production_at_start_time:
            **kwargs:

        Returns:

        """

        q_i = production_at_start_time
        b = kwargs.pop('degree_of_curvature')
        D_i = kwargs.pop('decline_rate')

        # Validate parameters

        if q_i <= 0:
            raise ValueError('Production rate can never be less than or equal to zero.')

        if b < 0 or b > 1:
            raise ValueError('degree of curvature but be between 0 and 1, inclusive.')

        if D_i <= 0:
            raise ValueError('Decline rate must be strictly positive.')

        if time_start > time_end:
            raise ValueError('time_stop must be greater than or equal to time_start.')

        # compute the time array over whih the decline curve will be computed

        time_vector = np.linspace(start=time_start,
                                  stop=time_end,
                                  num=100)

        # hand special cases
        # TODO: b == 0 needs to be computed with a threshold, computationally.

        if b is 0:

            decline_curve = q_i * np.exp(-D_i * time_vector)

        elif b is 1:

            decline_curve = q_i / (10. + D_i * time_vector)

        else:

            decline_curve = q_i / (1.0 + b * D_i * time_vector)**(1/b)

        return time_vector, decline_curve

#
# if __name__ == '__main__':
#
#     production_at_start_time = 100.0
#     degree_of_curvature = 0
#     decline_rate = .1
#
#     time_start = 10
#     time_end = 100
#
#     decline_engine = Arps()
#
#     curve = decline_engine.compute(time_start=time_start,
#                                    time_end=time_end,
#                                    production_at_start_time=production_at_start_time,
#                                    degree_of_curvature=degree_of_curvature,
#                                    decline_rate=decline_rate)
#
#     import matplotlib.pyplot as plt
#
#     plt.figure()
#     plt.scatter(curve[0], curve[1])
#     plt.show()
